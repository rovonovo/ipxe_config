# ipxe_config

Booting Ubuntu Live Disc

To be able to boot into the Live Disc of the Ubuntu distribution, there are several requirements:

    The Ubuntu iso
    An NFS daemon
    Typical network booting infrastructure, including but not limited to:
        Physical layer
        DHCP
        DNS

The Ubuntu iso - Ubuntu desktop 18.04 LTS

 dnsmasq is used when there is an existing DHCP service on the network that must continue to be used and cannot be altered to support . dnsmasq is a form of Proxy DHCP. It listens for DHCP requests (from hosts) and responses (from dhcp service). When a request and response is heard, dnsmasq "adds to" the response.

How ProxyDHCP works

    When a PXE client boots up, it sends a DHCP Discover broadcast on the network, which includes a list of information the client would like from the DHCP server, and some information identifying itself as a PXE capable device.
    A regular DHCP server responds with a DHCP Offer, which contains possible values for network settings requested by the client. Usually a possible IP address, subnet mask, router (gateway) address, dns domain name, etc.
    Because the client identified itself as a PXEClient, the proxyDHCP server also responds with a DHCP Offer with additional information, but not IP address info. It leaves the IP address assigning to the regular DHCP server. The proxyDHCP server provides the next-server-name and boot file name values, which is used by the client during the upcoming TFTP transaction.
    The PXE Client responds to the DHCP Offer with a DHCP Request, where it officially requests the IP configuration information from the regular DHCP server.
    The regular DHCP server responds back with an ACK (acknowledgement), letting the client know it can use the IP configuration information it requested.
    The client now has its IP configuration information, TFTP Server name, and boot file name and it initiate a TFTP transaction to download the boot file.


dnsmasq port in use error can be bypassed by setting up a static ip in the interface


Preparing the File Structure

First, download the latest version of the Ubuntu variant of choice. Extract the iso file using your compression software of choice. All of the iso file's contents will be required, as this entire directory is going to be mounted to a client whenever it is booted to the Ubuntu environment.

Your directory structure may look like this: 

ls -l /media/nfs/
total 8220
dr-xr-xr-x 3 root root    4096 Jan 28 20:55 boot
-rw-r--r-- 2 root root    1312 Jan 28 21:44 boot.ipxe
dr-xr-xr-x 2 root root    4096 Jan 28 20:55 casper
dr-xr-xr-x 3 root root    4096 Jan 28 20:55 dists
dr-xr-xr-x 3 root root    4096 Jan 28 20:55 EFI
dr-xr-xr-x 2 root root    4096 Jan 28 20:55 install
dr-xr-xr-x 2 root root   12288 Jan 28 20:55 isolinux
-rw-r--r-- 1 root root 8330904 Jan 28 20:55 linux
-r--r--r-- 1 root root   23882 Jan 28 20:55 md5sum.txt
dr-xr-xr-x 2 root root    4096 Jan 28 20:55 pics
dr-xr-xr-x 4 root root    4096 Jan 28 20:55 pool
dr-xr-xr-x 2 root root    4096 Jan 28 20:55 preseed
-rw-r--r-- 1 root root    5367 Jan 28 20:55 preseed.cfg
-r--r--r-- 1 root root     233 Jan 28 20:55 README.diskdefines

Preparing the NFS Share
Installing the Package

The NFS daemon package must be installed according to your distribution's instructions.

    Ubuntu
    Debian

Configuring the Share

Edit the /etc/exports file using your editor of choice. Apply the following line, substituting in the path where you will host your Ubuntu file structure

  /path/to/ubuntu/files     *(ro,sync,no_wdelay,insecure_locks,no_root_squash,insecure)

Note: The * prefix before those options specifies that this share is publicly accessible with access to the network resource. This can be changed to fit any security requirements.


iPXE Booting

For the boot to occur the following items must be supplied:

    Kernel
    Initial ramdisk
    NFS options
    Client IP information

Note: This configuration assumes the iPXE boot file you are using has the DOWNLOAD_PROTO_NFS option enabled. If it doesn't, you will have to change the kernel and initrd lines to boot over a protocol that is supported in your environment. Either this, or you could rebuild your iPXE boot file.

Compiling IPXE to support NFS

Source code

The iPXE source code is maintained in a git repository at http://git.ipxe.org/ipxe.git. You can check out a copy of the code using:

  git clone http://git.ipxe.org/ipxe.git

and build it using:

  cd ipxe/src
  make

You will need to have at least the following packages installed in order to build iPXE:

    gcc (version 3 or later)
    binutils (version 2.18 or later)
    make
    perl
    liblzma or xz header files     --liblzma for debian
    mtools
    mkisofs (needed only for building .iso images)
    syslinux (for isolinux, needed only for building .iso images)


Chainloading from an existing PXE ROM

A chain

You can chainload iPXE from an existing PXE ROM. This is useful if you have a large number of machines that you want to be able to boot using iPXE, but you do not want to reflash the network card on each individual machine.

You can build a chainloadable iPXE image using:

  make bin/undionly.kpxe
	
